package com.nike.cardshuffler;

import java.util.Collections;

import com.nike.cardshuffler.model.Deck;

public class RandomShuffler implements Shuffler {

	@Override
	public void shuffle(Deck deck) {
		Collections.shuffle(deck.getCards());
	}

}
