package com.nike.cardshuffler.model;

public enum Suit {
	CLUBS, DIAMONDS, HEARTS, SPADES;
}
