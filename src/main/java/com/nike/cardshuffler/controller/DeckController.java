package com.nike.cardshuffler.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nike.cardshuffler.model.Deck;
import com.nike.cardshuffler.service.DeckService;

@Controller
@RequestMapping(value = "/ap1/v1/decks/")
public class DeckController {

	protected DeckService deckService;

	@Autowired
	public DeckController(DeckService deckService) {
		super();
		this.deckService = deckService;
	}

	@RequestMapping(value = "/{deckName}", method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<Deck> findOne(@PathVariable String deckName) {
		Deck deck = deckService.findOne(deckName);
		return new ResponseEntity<Deck>(deck, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody ResponseEntity<List<Deck>> findAll() {
		Collection<Deck> dtoList = deckService.findAll();
		return new ResponseEntity<List<Deck>>(new ArrayList<Deck>(dtoList), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public @ResponseBody ResponseEntity<Deck> create(@RequestBody String deckName) {
		Deck deck = deckService.create(deckName);
		return new ResponseEntity<Deck>(deck, HttpStatus.OK);
	}

	@RequestMapping(value = "/{deckName}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Deck> delete(@PathVariable String deckName) {
		Deck deck = deckService.delete(deckName);
		return new ResponseEntity<Deck>(deck, HttpStatus.OK);
	}

	@RequestMapping(value = "/{deckName}/shuffle", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Deck> shuffle(@PathVariable String deckName) {
		Deck deck = deckService.shuffle(deckName);
		return new ResponseEntity<Deck>(deck, HttpStatus.OK);
	}

}
