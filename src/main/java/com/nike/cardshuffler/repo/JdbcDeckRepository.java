package com.nike.cardshuffler.repo;

import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nike.cardshuffler.model.Deck;

@Repository
@Profile(value="production")
public interface JdbcDeckRepository extends DeckRepository, JpaRepository<Deck, String> {

}
