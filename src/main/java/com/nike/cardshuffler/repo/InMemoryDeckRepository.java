package com.nike.cardshuffler.repo;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.nike.cardshuffler.model.Deck;

@Service
@Profile("dev")
public class InMemoryDeckRepository implements DeckRepository {

	private final ConcurrentHashMap<String, Deck> decks = new ConcurrentHashMap<String, Deck>();

	@Override
	public Deck findOne(String name) {
		return decks.get(name);
	}

	@Override
	public Collection<Deck> findAll() {
		return Collections.unmodifiableCollection(decks.values());
	}

	@Override
	public Deck save(Deck deck) {
		decks.put(deck.getName(), deck);
		return deck;
	}

	@Override
	public void delete(String name) {
		decks.remove(name);
	}
}
