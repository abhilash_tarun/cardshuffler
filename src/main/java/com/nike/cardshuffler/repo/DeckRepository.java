package com.nike.cardshuffler.repo;

import java.util.Collection;

import com.nike.cardshuffler.model.Deck;

public interface DeckRepository {

	Deck findOne(String name);

	Collection<Deck> findAll();

	Deck save(Deck deck);

	void delete(String name);
}
