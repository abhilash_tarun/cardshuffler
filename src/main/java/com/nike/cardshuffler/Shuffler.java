package com.nike.cardshuffler;

import com.nike.cardshuffler.model.Deck;

public interface Shuffler {

	void shuffle(Deck deck);

}
