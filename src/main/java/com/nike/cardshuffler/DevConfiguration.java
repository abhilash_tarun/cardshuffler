package com.nike.cardshuffler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

import com.nike.cardshuffler.repo.DeckRepository;
import com.nike.cardshuffler.repo.InMemoryDeckRepository;
import com.nike.cardshuffler.service.DeckService;
import com.nike.cardshuffler.service.DeckServiceImpl;

@Configuration()
@Profile("dev")
public class DevConfiguration {

	@Autowired
	private Environment env;

	@Bean
	public DeckService deckService() {
		final DeckServiceImpl deckService = new DeckServiceImpl();
		deckService.setDeckRepository(deckRepo());
		deckService.setShuffler(randomShuffler());
		return deckService;
	}

	@Bean
	@Primary
	public DeckRepository deckRepo() {
		return new InMemoryDeckRepository();
	}

	@Bean
	@Primary
	public Shuffler randomShuffler() {
		return new RandomShuffler();
	}

	@Bean
	public Shuffler handShuffler() {
		return new HandShuffler();
	}
}
