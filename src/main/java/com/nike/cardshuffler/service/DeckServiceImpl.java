package com.nike.cardshuffler.service;

import java.util.Collection;
import java.util.Comparator;

import org.springframework.stereotype.Service;

import com.nike.cardshuffler.Shuffler;
import com.nike.cardshuffler.model.Card;
import com.nike.cardshuffler.model.Deck;
import com.nike.cardshuffler.repo.DeckRepository;

@Service
public class DeckServiceImpl implements DeckService {

	private DeckRepository deckRepository;

	private Shuffler shuffler;

	public DeckRepository getDeckRepository() {
		return deckRepository;
	}

	public void setDeckRepository(DeckRepository deckRepository) {
		this.deckRepository = deckRepository;
	}

	public Shuffler getShuffler() {
		return shuffler;
	}

	public void setShuffler(Shuffler shuffler) {
		this.shuffler = shuffler;
	}

	@Override
	public Deck create(String deckName) {
		Deck deck = new DeckBuilder().withName(deckName).create();
		return deckRepository.save(deck);
	}

	@Override
	public Deck delete(String deckName) {
		Deck deckToDelete = findOne(deckName);
		if (deckToDelete != null) {
			deckRepository.delete(deckName);
		}
		return deckToDelete;
	}

	@Override
	public Deck findOne(String deckName) {
		return deckRepository.findOne(deckName);
	}

	@Override
	public Deck findByName(String deckName) {
		return deckRepository.findOne(deckName);
	}

	@Override
	public Collection<Deck> findAll() {
		return deckRepository.findAll();
	}

	@Override
	public Deck shuffle(String deckName) {
		Deck deck = findOne(deckName);
		if (deck != null) {
			shuffler.shuffle(deck);
		}
		return deck;
	}

	public static class AscOrderCardComparator implements Comparator<Card> {

		@Override
		public int compare(Card o1, Card o2) {
			if (o1.getSuit().equals(o2.getSuit())) {
				// compare ranks
				if (o1.getRank().ordinal() > o2.getRank().ordinal()) {
					return 1;
				} else if (o1.getRank().ordinal() < o2.getRank().ordinal()) {
					return -1;
				} else {
					return 0;
				}
			} else {
				if (o1.getSuit().ordinal() > o2.getSuit().ordinal()) {
					return 1;
				} else if (o1.getSuit().ordinal() < o2.getSuit().ordinal()) {
					return -1;
				} else {
					return 0;
				}
			}
		}
	}

	public static class DescOrderCardComparator implements Comparator<Card> {

		@Override
		public int compare(Card o1, Card o2) {
			if (o1.getSuit().equals(o2.getSuit())) {
				// compare ranks
				if (o1.getRank().ordinal() > o2.getRank().ordinal()) {
					return -1;
				} else if (o1.getRank().ordinal() < o2.getRank().ordinal()) {
					return 1;
				} else {
					return 0;
				}
			} else {
				if (o1.getSuit().ordinal() > o2.getSuit().ordinal()) {
					return -1;
				} else if (o1.getSuit().ordinal() < o2.getSuit().ordinal()) {
					return 1;
				} else {
					return 0;
				}
			}
		}
	}

}
