package com.nike.cardshuffler.service;

import java.util.Collection;

import org.springframework.stereotype.Service;

import com.nike.cardshuffler.model.Deck;

@Service
public interface DeckService {

	public Deck create(String name);

	public Deck delete(String name);

	public Deck findOne(String deckId);

	public Deck findByName(String name);

	public Collection<Deck> findAll();

	public Deck shuffle(String deckName);
}
