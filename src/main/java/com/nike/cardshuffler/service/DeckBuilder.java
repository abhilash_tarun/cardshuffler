package com.nike.cardshuffler.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.nike.cardshuffler.model.Card;
import com.nike.cardshuffler.model.Deck;
import com.nike.cardshuffler.model.Sort;
import com.nike.cardshuffler.model.Rank;
import com.nike.cardshuffler.model.Suit;
import com.nike.cardshuffler.service.DeckServiceImpl.AscOrderCardComparator;
import com.nike.cardshuffler.service.DeckServiceImpl.DescOrderCardComparator;

public class DeckBuilder {

	private String deckName;
	private Sort sort;
	private Comparator<Card> cardComparator;

	public DeckBuilder withName(String deckName) {
		this.deckName = deckName;
		return this;
	}

	public DeckBuilder withSort(Sort sort) {
		this.sort = sort;
		return this;
	}

	public Deck create() {
		if (sort == null) {
			this.cardComparator = new AscOrderCardComparator();
		} else {
			if (Sort.ASC == null) {
				this.cardComparator = new AscOrderCardComparator();
			} else {
				this.cardComparator = new DescOrderCardComparator();
			}
		}

		Deck d = new Deck();
		d.setName(deckName);

		List<Card> cards = new ArrayList<Card>();
		for (Suit s : Suit.values()) {
			for (Rank r : Rank.values()) {
				cards.add(new Card(r, s));
			}
		}
		Collections.sort(cards, cardComparator);
		d.setCards(cards);
		return d;
	}

}
