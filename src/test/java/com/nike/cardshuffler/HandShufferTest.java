package com.nike.cardshuffler;

import org.junit.Test;

import com.nike.cardshuffler.model.Deck;
import com.nike.cardshuffler.service.DeckBuilder;

public class HandShufferTest {

	@Test
	public void testHandShuffling() {

		Deck deck = new DeckBuilder().withName("FirstDesk").create();

		HandShuffler hShuffler = new HandShuffler();
		hShuffler.shuffle(deck);
	}

}
